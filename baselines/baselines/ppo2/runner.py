import numpy as np
from baselines.common.runners import AbstractEnvRunner


class Runner(AbstractEnvRunner):
    """
    We use this object to make a mini batch of experiences
    __init__:
    - Initialize the runner

    run():
    - Make a mini batch
    """
    def __init__(self, *, env, model, nsteps, gamma, lam):
        super().__init__(env=env, model=model, nsteps=nsteps)
        # Lambda used in GAE (General Advantage Estimation)
        self.lam = lam
        # Discount rate
        self.gamma = gamma

    def run(self, run_type='attacked'):
        """
        :param run_type: if unattacked, run \pi(a|s), else run \pi(a|s+\delta)
        """
        assert run_type == 'attacked' or run_type == 'unattacked', 'unsupported running type'
        # print("check nsteps in runner:", self.nsteps)
        # exit(0)
        # Here, we init the lists that will contain the mb of experiences
        if run_type == 'attacked':
            mb_obs, mb_obs_delta, mb_rewards, mb_actions, mb_values, mb_dones, mb_neglogpacs, mb_cro_init, mb_cro_term, mb_policy_entropy_unattacked\
                = [],[],[],[],[],[],[],[],[],[]
            mb_states = self.states
            epinfos = []
            # For n in range number of steps
            for _ in range(self.nsteps):
                # Given observations, get action value and neglopacs
                # We already have self.obs because Runner superclass run self.obs[:] = env.reset() on init
                ############################################################################################################
                # observations and values are unattacked, but the neglogpacs are attacked: \pi(a|s+\delta)
                actions, values, self.states, neglogpacs, obs_delta, cro_ent_loss_init, cro_ent_loss_term, policy_entropy_unattacked \
                    = self.model.step(self.obs, S=self.states, M=self.dones)
                # print("cros", cro_ent_loss_init)
                # exit(0)
                # print("att runner check", np.shape(actions), type(self.obs), np.shape(self.obs[:]))
                # exit(0)
                mb_obs.append(self.obs.copy())
                mb_obs_delta.append(obs_delta.copy())
                mb_actions.append(actions)
                mb_values.append(values)
                mb_neglogpacs.append(neglogpacs)
                mb_dones.append(self.dones)
                mb_cro_init.append([cro_ent_loss_init])
                mb_cro_term.append([cro_ent_loss_term])
                mb_policy_entropy_unattacked.append([policy_entropy_unattacked])

                # Take actions in env and look the results
                # Infos contains a ton of useful informations
                # print("runner check, policy type", actions, values, neglogpacs)
                self.obs[:], rewards, self.dones, infos = self.env.step(actions)
                for info in infos:
                    maybeepinfo = info.get('episode')
                    if maybeepinfo: epinfos.append(maybeepinfo)
                mb_rewards.append(rewards)
            # batch of steps to batch of rollouts
            mb_obs = np.asarray(mb_obs, dtype=self.obs.dtype)
            mb_obs_delta = np.asarray(mb_obs_delta, dtype=obs_delta.dtype)

            mb_rewards = np.asarray(mb_rewards, dtype=np.float32)
            mb_actions = np.asarray(mb_actions)
            mb_values = np.asarray(mb_values, dtype=np.float32)
            mb_neglogpacs = np.asarray(mb_neglogpacs, dtype=np.float32)
            mb_cro_init = np.asarray(mb_cro_init, dtype=np.float32)
            mb_cro_term = np.asarray(mb_cro_term, dtype=np.float32)
            mb_policy_entropy_unattacked = np.array(mb_policy_entropy_unattacked, dtype=np.float32)
            mb_dones = np.asarray(mb_dones, dtype=np.bool)
            # last_values = self.model.value(self.obs, S=self.states, M=self.dones)
            _, last_values, _, _, _, _, _, _ = self.model.step(self.obs, S=self.states, M=self.dones)

            # discount/bootstrap off value fn
            mb_advs = np.zeros_like(mb_rewards)
            lastgaelam = 0
            for t in reversed(range(self.nsteps)):
                if t == self.nsteps - 1:
                    nextnonterminal = 1.0 - self.dones
                    nextvalues = last_values
                else:
                    nextnonterminal = 1.0 - mb_dones[t+1]
                    nextvalues = mb_values[t+1]
                delta = mb_rewards[t] + self.gamma * nextvalues * nextnonterminal - mb_values[t]
                mb_advs[t] = lastgaelam = delta + self.gamma * self.lam * nextnonterminal * lastgaelam
            mb_returns = mb_advs + mb_values
            # print("info", mb_neglogpacs, mb_cro_init)
            return (*map(sf01, (mb_obs, mb_obs_delta, mb_returns, mb_dones, mb_actions, mb_values, mb_neglogpacs,
                                mb_cro_init, mb_cro_term, mb_policy_entropy_unattacked)),
                mb_states, epinfos)
        else:
            mb_obs, mb_rewards, mb_actions, mb_values, mb_dones, mb_neglogpacs = [],[],[],[],[],[]
            epinfos = []
            mb_states = self.states
            for _ in range(self.nsteps):
                # print("steps in runner unattacked", self.nsteps)
                actions, values, self.states, neglogpacs = self.model.step_unattacked(self.obs, S=self.states, M=self.dones)
                # print("unattacked shape check", np.shape(actions), np.shape(values), np.shape(neglogpacs))
                # exit(0)
                mb_obs.append(self.obs.copy())
                mb_actions.append(actions)
                mb_values.append(values)
                mb_dones.append(self.dones)
                mb_neglogpacs.append(neglogpacs)
                # print("runner check", actions, values, neglogpacs)
                # exit(0)
                self.obs[:], rewards, self.dones, infos = self.env.step(actions)
                # self.env.render()
                # print("environment stepped")
                # exit(0)
                for info in infos:
                    maybeepinfo = info.get('episode')
                    if maybeepinfo: epinfos.append(maybeepinfo)
                mb_rewards.append(rewards)
                # print("episode info attacked")
            # batch of steps to batch of rollouts
            # print("out of for cycle")
            mb_obs = np.asarray(mb_obs, dtype=self.obs.dtype)
            # print("obs array")
            mb_rewards = np.asarray(mb_rewards, dtype=np.float32)
            # print("reward array")
            mb_actions = np.asarray(mb_actions)
            # print("action array")
            mb_values = np.asarray(mb_values, dtype=np.float32)
            # print("value array")
            mb_dones = np.asarray(mb_dones, dtype=np.bool)
            # print("done array")
            mb_neglogpacs = np.asarray(mb_neglogpacs, dtype=np.float32)
            # print("batch data prepared, next calculate last value")
            _, last_values, _, _ = self.model.step_unattacked(self.obs, S=self.states, M=self.dones)

            mb_advs = np.zeros_like(mb_rewards)
            lastgaelam = 0
            for t in reversed(range(self.nsteps)):
                if t == self.nsteps - 1:
                    nextnonterminal = 1.0 - self.dones
                    nextvalues = last_values
                else:
                    nextnonterminal = 1.0 - mb_dones[t + 1]
                    nextvalues = mb_values[t + 1]
                delta = mb_rewards[t] + self.gamma * nextvalues * nextnonterminal - mb_values[t]
                mb_advs[t] = lastgaelam = delta + self.gamma * self.lam * nextnonterminal * lastgaelam
            mb_returns = mb_advs + mb_values
            # print("check in runner, everything is ok!")
            return (*map(sf01, (mb_obs, mb_returns, mb_dones, mb_actions, mb_values, mb_neglogpacs)), mb_states, epinfos)


def sf01(arr):
    """
    swap and then flatten axes 0 and 1
    """
    s = arr.shape
    return arr.swapaxes(0, 1).reshape(s[0] * s[1], *s[2:])


